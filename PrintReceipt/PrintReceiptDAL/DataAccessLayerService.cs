﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintReceiptDAL
{
    public static class DataAccessLayerService
    {
        public static IBankRepository BankInfoRepository()
        {
            IBankRepository obj = new bankRepository();
            return obj;
        }
        public static IBranchRepository BranchInfoRepository()
        {
            IBranchRepository obj = new branchRepository();
            return obj;
        }
        public static IReceiptRepository ReceiptInfoRepository()
        {
            IReceiptRepository obj = new receiptRepository();
            return obj;
        }
        public static IReceiptDetailRepository ReceiptDetailInfoRepository()
        {
            IReceiptDetailRepository obj = new receiptDetailRepository();
            return obj;
        }
        public static ICustomerRepository CustomerInfoRepository()
        {
            ICustomerRepository obj = new customerRepository();
            return obj;
        }
        public static IReceiptNoRepository ReceiptNoInfoRepository()
        {
            IReceiptNoRepository obj = new receiptNoRepository();
            return obj;
        }

    }
}
