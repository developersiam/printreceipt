﻿using DomainModel;

namespace PrintReceiptDAL
{
    public interface IBankRepository : IGenericDataRepository<bank> { }
    public class bankRepository : GenericDataRepository<bank>, IBankRepository { }
    public interface IBranchRepository : IGenericDataRepository<branch> { }
    public class branchRepository : GenericDataRepository<branch>, IBranchRepository { }
    public interface IReceiptRepository : IGenericDataRepository<receipt> { }
    public class receiptRepository : GenericDataRepository<receipt>, IReceiptRepository { }
    public interface IReceiptDetailRepository : IGenericDataRepository<receipt_detail> { }
    public class receiptDetailRepository : GenericDataRepository<receipt_detail>, IReceiptDetailRepository { }
    public interface ICustomerRepository : IGenericDataRepository<cus_rcp> { }
    public class customerRepository : GenericDataRepository<cus_rcp>, ICustomerRepository { }
    public interface IReceiptNoRepository : IGenericDataRepository<receipt_no> { }
    public class receiptNoRepository : GenericDataRepository<receipt_no>, IReceiptNoRepository { }
}
