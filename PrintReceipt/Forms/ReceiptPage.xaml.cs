﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using PrintReceiptBL;
using PrintReceiptDAL;
using DomainModel;

namespace PrintReceipt.Forms
{
    /// <summary>
    /// Interaction logic for ReceiptPage.xaml
    /// </summary>
    public partial class ReceiptPage : Page
    {
        //bool chckBox = false;
        Int16 cashChq;
        Int16 transfer;
        string newRcno = "";
        string bankId = "";
        string branchId = "";

        public Microsoft.Office.Interop.Excel.Application excelApplication = new Microsoft.Office.Interop.Excel.Application();
        public Microsoft.Office.Interop.Excel.Workbook excelWorkBook;
        public Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet;
        public Microsoft.Office.Interop.Excel.Range excelRange;
        public ReceiptPage()
        {
            InitializeComponent();
            BlindingData();
        }

        private void BlindingData()
        {
            try
            {
                var b = new List<string>();
                
                EndDatePicker.Text = DateTime.Now.ToShortDateString();
                ReceiptDataGrid.ItemsSource = null;
                //Add Bank
                b = BussinessLayerService.BankInfo().GetBankName();
                b.Insert(0, "None");
                BankComboBox.ItemsSource = b;
                //Add Branch
                b = new List<string>();
                b = BussinessLayerService.BranchInfo().GetBranchName();
                b.Insert(0, "None");
                BranchComboBox.ItemsSource = b;
                //Reciept No.
                ReceiptComboBox.ItemsSource = BussinessLayerService.ReceiptInfo().GetReceiptNo();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void ReceiptComboBox_DropDownClosed(object sender, EventArgs e)
        {
            //clear
            ClearForm();

            //Get receipt data
            if (ReceiptComboBox.Text != "")
            {
                GetAllReceiptInfo(ReceiptComboBox.Text);
            }

            SearchCusButton.IsEnabled = false;
            //addItemButton.IsEnabled = false;
            //deleteItemButton.IsEnabled = false;

        }

        private void GetAllReceiptInfo(string selectedRCno)
        {
            DateTime rcDate;
            //receipt
            var existedReceipt = BussinessLayerService.ReceiptInfo().GetReceiptInfo(selectedRCno);
            {
                ReceiptComboBox.Text = selectedRCno;
                rcDate = existedReceipt.dd.HasValue ? existedReceipt.dd.Value : DateTime.Now;
                EndDatePicker.Text = rcDate.ToString("dd/MM/yyyy");
                ChequeNoTextBox.Text = existedReceipt.chq_no;
                TransferDetailTextBox.Text = existedReceipt.transfer_detail;
                if (existedReceipt.cash_chq != 0) //cash_chq = -1 and transfer = 0
                    PaidOptCombo.Text = "CASH";
                else if (existedReceipt.transfer == -1) //cash_chq = 0 and transfer = -1
                    PaidOptCombo.Text = "TRANSFER (ยอดโอน)";
                else if (existedReceipt.cash_chq == 0) //cash_chq = 0 and transfer = 0
                    PaidOptCombo.Text = "CHEQUE";
                //customer
                if ((existedReceipt.cus_id != null) || (existedReceipt.cus_id != ""))
                {
                    CusIDTextBox.Text = existedReceipt.cus_id;
                    var CustomerInfo = BussinessLayerService.CustomerInfo().GetCustomerInfo(existedReceipt.cus_id);
                    NameTextBox.Text = CustomerInfo.name;
                    TaxTextBox.Text = CustomerInfo.taxid;
                    Address1TextBox.Text = string.Concat(CustomerInfo.homeid, " ", CustomerInfo.soi, " ", CustomerInfo.moo, " ", CustomerInfo.road);
                    Address2TextBox.Text = string.Concat(CustomerInfo.tumbol, " ", CustomerInfo.amphur, " ", CustomerInfo.province, " ", CustomerInfo.postal);
                }

                NormalCheckBox.IsChecked = existedReceipt.status == true ? true : false;
                DenieBox.IsChecked = existedReceipt.status == false ? true : false;
                //if (existedReceipt.status)
                //{
                //    NormalCheckBox.IsChecked = true;
                //    DenieBox.IsChecked = false;
                //}
                //else
                //{
                //    NormalCheckBox.IsChecked = false;
                //    DenieBox.IsChecked = true;
                //}
                //bank
                if ((existedReceipt.bank_id != null) && (existedReceipt.bank_id != ""))
                {
                    var BankInfo = BussinessLayerService.BankInfo().GetBankInfo(existedReceipt.bank_id);
                    BankComboBox.Text = BankInfo.name.ToString();
                }
                //branch
                if ((existedReceipt.branch_id != null) && (existedReceipt.branch_id != ""))
                {
                    var BranchInfo = BussinessLayerService.BranchInfo().GetBranchInfo(existedReceipt.branch_id);
                    BranchComboBox.Text = BranchInfo.name.ToString();
                }
            }
            //receipt_details
            ReceiptDataGrid.ItemsSource = BussinessLayerService.ReceiptDetailInfo().GetReceiptDetailInfo(selectedRCno);
        }

        private void ClearForm()
        {
            ReceiptDataGrid.ItemsSource = null;
            NoTextBox.Text = null;  
            DetailTextBox.Text = null;
            AmtTextBox.Text = null;
            UnitPriceTextBox.Text = null;
            TotalTextBox.Text = null;
            NormalCheckBox.IsChecked = true;
            DenieBox.IsChecked = false;

            BankComboBox.Text = "";
            BranchComboBox.Text = "";
            ChequeNoTextBox.Text = "";

            //Customer
            CusIDTextBox.Text = null;
            NameTextBox.Text = null;
            TaxTextBox.Text = null;
            Address1TextBox.Text = null;
            Address2TextBox.Text = null;

        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
            BlindingData();
            EnableNewReceipt();
        }

        private void EnableNewReceipt()
        {
            ReceiptComboBox.Text = "";
            ReceiptComboBox.IsEnabled = false;
            SearchCusButton.IsEnabled = true;
            addItemButton.IsEnabled = true;
            deleteItemButton.IsEnabled = true;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
            BlindingData();
        }

        private void SearchCusButton_Click(object sender, RoutedEventArgs e)
        {

            CustomerWindow fd = new CustomerWindow();
            fd.ShowDialog();

            if (fd.Cus_selected == null) return;
            CusIDTextBox.Text = fd.Cus_selected.cus_id;
            NameTextBox.Text = fd.Cus_selected.name;
            TaxTextBox.Text = fd.Cus_selected.taxid;
            Address1TextBox.Text = fd.Cus_selected.homeid + " " + fd.Cus_selected.soi + " " + fd.Cus_selected.moo + " " + fd.Cus_selected.road;
            Address2TextBox.Text = fd.Cus_selected.tumbol + " " + fd.Cus_selected.amphur + " " + fd.Cus_selected.province + " " + fd.Cus_selected.postal;
        }

        private void ReceiptDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (ReceiptDataGrid.SelectedIndex < 0)
                    return;

                var selected = (receipt_detail)ReceiptDataGrid.SelectedItem;
                if (selected != null)
                {
                    NoTextBox.SelectedText = selected.no.ToString();
                    DetailTextBox.SelectedText = selected.des;
                    AmtTextBox.SelectedText = string.Format("{0:N2}", selected.quantity);
                    UnitPriceTextBox.SelectedText = string.Format("{0:N2}", selected.punit);
                    TotalTextBox.SelectedText = string.Format("{0:N2}", selected.amount);
                };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void addItemButton_Click(object sender, RoutedEventArgs e)
        {        
            //Check Existed Receipt
            if (ReceiptComboBox.Text != "")
            {
                //---------------------------------  Edit receipt ---------------------------------------------
                if (BussinessLayerService.ReceiptInfo().ExistedReceipt(ReceiptComboBox.Text))
                {
                    UpdateReceiptInfo();
                    MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "Receipt System", MessageBoxButton.OK, MessageBoxImage.Information);

                    //Blinding New Info
                    BlindingData();
                    GetAllReceiptInfo(ReceiptComboBox.Text);
                    
                }                   
            }
            else
            {
                //---------------------------------- Add New to receipt ---------------------------------------
                if (CheckReceiptDetails())
                {
                    //Add new receipt info
                    AddReceiptInfo();
                    //Update Detail
                    UpdateReceiptDetail(newRcno);
                    MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "Receipt System", MessageBoxButton.OK, MessageBoxImage.Information);

                    //Blinding New Info
                    BlindingData();
                    GetAllReceiptInfo(newRcno);
                }                                              
             }
        }        

        private void AddReceiptInfo()
        {
            //Verify all Info
            VerifyAllInfo();
            //Get new rc no
            receipt_no rc_no = BussinessLayerService.ReceiptNoInfo().GetReceiptNo();
            if (rc_no != null)
            {
                int tmprcno = Convert.ToInt32(rc_no.receipt_no1) + 1;
                //Add receipt
                var newItem = new receipt
                {
                    receipt_no = string.Concat(rc_no.yy.Substring(2, 2), string.Format("{0:D4}", tmprcno)),
                    yy = rc_no.yy,
                    mm = EndDatePicker.SelectedDate.Value.Month.ToString(),
                    cus_id = CusIDTextBox.Text,
                    dd = EndDatePicker.SelectedDate.Value,
                    cash_chq = cashChq,
                    bank_id = bankId,
                    branch_id = branchId,
                    chq_no = ChequeNoTextBox.Text,
                    remark = "",
                    status = NormalCheckBox.IsChecked == true ? true : false,
                    recorddt = DateTime.Now,
                    transfer = transfer,
                    transfer_detail = TransferDetailTextBox.Text
                };
                BussinessLayerService.ReceiptInfo().AddNewReceipt(newItem);

                //Update Receipt_no
                UpdateReceiptNo(rc_no.yy, string.Format("{0:D4}", tmprcno));
                newRcno = newItem.receipt_no;
            }
        }

        private void UpdateReceiptNo(string ryy, string v)
        {
            var rcno = new receipt_no
            {
                yy = ryy,
                receipt_no1 = v,
                recorddt = DateTime.Now
            };
            BussinessLayerService.ReceiptNoInfo().UpdateReceiptNo(rcno);
        }

        private void UpdateReceiptInfo()
        {
            //Verify all Info
            VerifyAllInfo();

            var ex = BussinessLayerService.ReceiptInfo().GetReceiptInfo(ReceiptComboBox.Text);
            //Update receipt
            var exItem = new receipt
            {
                receipt_no = ex.receipt_no,
                yy = ex.yy,
                mm = EndDatePicker.SelectedDate.Value.Month.ToString(),
                cus_id = CusIDTextBox.Text,
                dd = EndDatePicker.SelectedDate.Value,
                cash_chq = cashChq,
                bank_id = bankId,
                branch_id = branchId,
                chq_no = ChequeNoTextBox.Text,
                remark = "",
                status = NormalCheckBox.IsChecked == true ? true : false,
                recorddt = DateTime.Now,
                transfer = transfer,
                transfer_detail = TransferDetailTextBox.Text
            };
            BussinessLayerService.ReceiptInfo().UpdateReceipt(exItem);

            //Update Detail
            if (NoTextBox.Text != "") UpdateReceiptDetail(ex.receipt_no);
        }

        private void UpdateReceiptDetail(string rec_no)
        {
            string tmpAmt = "";
            int tmpqty = 0;
            decimal tmpunit = 0;
            tmpAmt = string.Format("{0:C0}", TotalTextBox.Text);
            //Add value for unit price and quatity
            if (AmtTextBox.Text != "" && UnitPriceTextBox.Text != "")
            {
                tmpqty = Convert.ToInt32(Math.Round(Convert.ToDecimal(AmtTextBox.Text), 0));
                tmpunit = Convert.ToDecimal(UnitPriceTextBox.Text);

                 var details = new receipt_detail
                {
                    receipt_no = rec_no,
                    no = Convert.ToByte(NoTextBox.Text),
                    des = DetailTextBox.Text,
                    quantity = (int)tmpqty,
                    punit = (decimal)tmpunit,
                    amount = (decimal)Convert.ToDecimal(tmpAmt),
                    recorddt = DateTime.Now
                };
                BussinessLayerService.ReceiptDetailInfo().AddReceiptDetails(details);
            }
            else
            {
                var details = new receipt_detail
                {
                    receipt_no = rec_no,
                    no = Convert.ToByte(NoTextBox.Text),
                    des = DetailTextBox.Text,
                    amount = (decimal)Convert.ToDecimal(tmpAmt),
                    recorddt = DateTime.Now
                };
                BussinessLayerService.ReceiptDetailInfo().AddReceiptDetails(details);
            }
                     
            NoTextBox.Text = null;
            DetailTextBox.Text = null;
            AmtTextBox.Text = null;
            UnitPriceTextBox.Text = null;
            TotalTextBox.Text = null;
        }

        private bool CheckReceiptDetails()
        {
            bool cf = true;
            if (string.IsNullOrEmpty(CusIDTextBox.Text))
            {
                MessageBox.Show("กรุณาเลือกรายชื่อลูกค้า", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                CusIDTextBox.Focus();
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(EndDatePicker.Text))
            {
                MessageBox.Show("กรุณาเลือกวันที่", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                EndDatePicker.Focus();
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(PaidOptCombo.Text))
            {
                MessageBox.Show("กรุณาเลือกประเภทการจ่ายเงิน", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                PaidOptCombo.Focus();
                cf = false;
                return cf;
            }

            if (string.IsNullOrEmpty(NoTextBox.Text))
            {
                MessageBox.Show("กรุณาใส่ลำดับของรายละเอียด", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                NoTextBox.Focus();
                cf = false;
                return cf;
            }
            
            if (string.IsNullOrEmpty(DetailTextBox.Text))
            {
                MessageBox.Show("กรุณาใส่รายละเอียด", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                DetailTextBox.Focus();
                cf = false;
                return cf;
            }

            if (string.IsNullOrEmpty(TotalTextBox.Text))
            {
                MessageBox.Show("กรุณาใส่ราคารวม", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                TotalTextBox.Focus();
                return cf;
            }
            return cf;
        }
        private void VerifyAllInfo()
        {
            cashChq = 0;
            transfer = 0;
            bankId = "";
            branchId = "";
            //if ((bool)(NormalCheckBox.IsChecked)) chckBox = true;
            if (BankComboBox.Text != "None") bankId = BussinessLayerService.BankInfo().GetBankIDByName(BankComboBox.Text);
            if (BranchComboBox.Text != "None") branchId = BussinessLayerService.BranchInfo().GetBranchIDByName(BranchComboBox.Text);

            if (PaidOptCombo.Text == "CASH") //cash_chq = -1 and transfer = 0
            {
                cashChq = -1;
                transfer = 0;
            }
            else if (PaidOptCombo.Text == "TRANSFER (ยอดโอน)") //cash_chq = 0 and transfer = -1
            {
                cashChq = 0;
                transfer = -1;
            }
            else if (PaidOptCombo.Text == "CHEQUE") //cash_chq = 0 and transfer = 0
            {
                cashChq = 0;
                transfer = 0;
            }
        }

        private void NormalCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            NormalCheckBox.IsChecked = true;
        }
        private void NormalCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            NormalCheckBox.IsChecked = false;
            if (DenieBox.IsChecked == false) DenieBox.IsChecked = true;
        }

        private void DenieBox_Checked(object sender, RoutedEventArgs e)
        {
            NormalCheckBox.IsChecked = false;
            if (DenieBox.IsChecked == false) DenieBox.IsChecked = true;
        }

        private void DenieBox_Unchecked(object sender, RoutedEventArgs e)
        {
            NormalCheckBox.IsChecked = true;
            if (DenieBox.IsChecked == true) DenieBox.IsChecked = false;
        }

        private void deleteItemButton_Click(object sender, RoutedEventArgs e)
        {
            string tmpAmt = "";
            int tmpqty = 0;
            decimal tmpunit = 0;
            tmpAmt = string.Format("{0:C0}", TotalTextBox.Text);
            //tmpqty = Convert.ToInt32(Math.Round(Convert.ToDecimal(AmtTextBox.Text), 0));

            if (AmtTextBox.Text != "" && UnitPriceTextBox.Text != "")
            {
                tmpqty = Convert.ToInt32(Math.Round(Convert.ToDecimal(AmtTextBox.Text), 0));
                tmpunit = Convert.ToDecimal(UnitPriceTextBox.Text);
                var details = new receipt_detail
                {
                    receipt_no = ReceiptComboBox.Text,
                    no = Convert.ToByte(NoTextBox.Text),
                    des = DetailTextBox.Text,
                    quantity = (int)(tmpqty),
                    punit = (decimal)(tmpunit),
                    amount = (decimal)Convert.ToDecimal(tmpAmt)
                };
                BussinessLayerService.ReceiptDetailInfo().DeletedReceiptDetails(details);
            }
            else
            {
                var details = new receipt_detail
                {
                    receipt_no = ReceiptComboBox.Text,
                    no = Convert.ToByte(NoTextBox.Text),
                    des = DetailTextBox.Text,
                    amount = (decimal)Convert.ToDecimal(tmpAmt)
                };
                BussinessLayerService.ReceiptDetailInfo().DeletedReceiptDetails(details);
            }
     
            BlindingData();
            GetAllReceiptInfo(ReceiptComboBox.Text);

            NoTextBox.Text = null;
            DetailTextBox.Text = null;
            AmtTextBox.Text = null;
            UnitPriceTextBox.Text = null;
            TotalTextBox.Text = null;

        }

        private void PrintCusButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PrintNoTextBox.Text == "") throw new Exception("กรุณาระบุจำนวนที่ต้องการ Print");
                if (ReceiptComboBox.Text != "")
                 {
                    var receiptDetail = BussinessLayerService.ReceiptDetailInfo().GetReceiptDetailInfo(ReceiptComboBox.Text);
                    Print(receiptDetail);
                 }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void Print(List<receipt_detail> selectedrec)
        {
            int run_row = 15;
            excelWorkBook = excelApplication.Workbooks.Open(@"C:\Report_Layout\Receipt v.0.4.xlsx");

            excelWorkSheet = excelWorkBook.ActiveSheet;
            excelWorkSheet.PageSetup.Zoom = false;
            excelWorkSheet.PageSetup.FitToPagesWide = 1;
            excelApplication.Visible = false;

            //Header
            PutDataintoCell("W6", ReceiptComboBox.Text.Substring(2, 4)); //เลขที่
            PutDataintoCell("F9", NameTextBox.Text + " (Tax ID: " + TaxTextBox.Text + ")");//ชื่อลูค้า
            PutDataintoCell("F10", Address1TextBox.Text + " " + Address2TextBox.Text); //ที่อยู่
            PutDataintoCell("F11", EndDatePicker.Text); //วันที่

            //Footer
            if (PaidOptCombo.Text == "CASH") PutDataintoCell("B30", "R");
            else if (PaidOptCombo.Text == "CHEQUE") PutDataintoCell("B31", "R");
            else if (PaidOptCombo.Text == "TRANSFER (ยอดโอน)")
            {
                PutDataintoCell("B33", "R");
                PutDataintoCell("F33", TransferDetailTextBox.Text);
            };
            PutDataintoCell("K31", BankComboBox.Text); //เลขที่
            PutDataintoCell("K32", ChequeNoTextBox.Text); //เลขที่
            PutDataintoCell("R31", BranchComboBox.Text); //เลขที่

            foreach (var i in selectedrec)
            {
                PutDataintoCell("A"+ run_row, i.no.ToString()); 
                PutDataintoCell("C" + run_row, i.des.ToString());//ชื่อ-สกุลพนักงาน
                PutDataintoCell("D" + run_row, i.quantity.ToString());//ตำแหน่ง

                PutDataintoCell("S" + run_row, i.punit.ToString()); //จำนวนวันทำงาน
                PutDataintoCell("V" + run_row, i.amount.ToString()); //จำนวนวันทำงานวันหยุด

                run_row += 1;
            }
            excelWorkSheet.PrintOutEx(1,1, PrintNoTextBox.Text);
            excelWorkBook.Close(0);
            excelApplication.Quit();

            MessageBox.Show("พิมพ์ใบเสร็จเรียบร้อยแล้ว", "Complete!", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void PutDataintoCell(string strCell, string value)
        {
            excelRange = excelWorkSheet.Range[strCell];
            excelWorkSheet.Range[strCell].Value = value;
        }
    }
}
