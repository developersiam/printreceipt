﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using PrintReceiptBL;
using PrintReceiptDAL;
using DomainModel;

namespace PrintReceipt.Forms
{
    /// <summary>
    /// Interaction logic for CustomerWindow.xaml
    /// </summary>
    public partial class CustomerWindow : Window
    {
        public cus_rcp Cus_selected = new cus_rcp();
        public CustomerWindow()
        {
            InitializeComponent();
            Blinding();
        }

        private void Blinding()
        {
            try
            {
                CusDataGrid.ItemsSource = null;
                CusDataGrid.ItemsSource = BussinessLayerService.CustomerInfo().GetAllCustomerInfo();
  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void CusDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (cus_rcp)CusDataGrid.SelectedItem;
                if (selected != null)
                {
                    Cus_selected = (cus_rcp)selected;
                }
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
