﻿using System;
using System.Windows;
using System.Windows.Controls;
using PrintReceiptBL;
using DomainModel;
using System.Windows.Input;

namespace PrintReceipt.Forms
{
    /// <summary>
    /// Interaction logic for BankPage.xaml
    /// </summary>
    public partial class BankPage : Page
    {
        public BankPage()
        {
            InitializeComponent();
            BlindData();
        }

        public void BlindData()
        {
            BankDataGrid.ItemsSource = null;
            var bank = BussinessLayerService.BankInfo().GetAllBankInfo();
            if (bank != null) BankDataGrid.ItemsSource = bank;
        }
        private void updateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Id.Text))
                {
                    var existed = BussinessLayerService.BankInfo().ExistedBank(Id.Text);
                    if (existed)
                    {
                        var existedBank = BussinessLayerService.BankInfo().GetBankInfo(Id.Text);
                        existedBank.name = BankName.Text;
                        existedBank.recorddt = DateTime.Now;
                        BussinessLayerService.BankInfo().UpdateBank(existedBank);
                    }
                }
                else
                {
                    var maxId = BussinessLayerService.BankInfo().GetMaxBankID();
                    Id.Text = maxId;
                    var newBank = new bank
                    {
                        bank_id = maxId,
                        name = BankName.Text,
                        remark = "",
                        recorddt = DateTime.Now
                    };
                    BussinessLayerService.BankInfo().AddNewBank(newBank);
                }

                BlindData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            Id.Text = "";
            BankName.Text = "";
            BlindData();
        }

        private void BankDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (bank)BankDataGrid.SelectedItem;
                if(selected != null)
                {
                    Id.Text = selected.bank_id;
                    BankName.Text = selected.name;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
