﻿using PrintReceiptBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using DomainModel;

namespace PrintReceipt.Forms
{

    public partial class BranchPage : Page
    {
        public BranchPage()
        {
            InitializeComponent();
            BlindData();
        }
        public void BlindData()
        {
            BranchDataGrid.ItemsSource = null;
            var branch = BussinessLayerService.BranchInfo().GetAllBranchInfo();
            if (branch != null) BranchDataGrid.ItemsSource = branch;
        }

        private void updateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Id.Text))
                {
                    if (BussinessLayerService.BranchInfo().ExistedBranch(Id.Text))
                    {
                        var existedBranch = BussinessLayerService.BranchInfo().GetBranchInfo(Id.Text);
                        existedBranch.name = BranchName.Text;
                        existedBranch.recorddt = DateTime.Now;
                        BussinessLayerService.BranchInfo().UpdateBranch(existedBranch);
                    }
                }
                else
                {
                    var maxId = BussinessLayerService.BranchInfo().GetMaxBranchID();
                    Id.Text = maxId;
                    var newBranch = new branch
                    {
                        branch_id = maxId,
                        name = BranchName.Text,
                        remark = "",
                        recorddt = DateTime.Now
                    };
                    BussinessLayerService.BranchInfo().AddNewBranch(newBranch);
                }

                BlindData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            Id.Text = "";
            BranchName.Text = "";
            BlindData();
        }

        private void BranchDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (branch)BranchDataGrid.SelectedItem;
                if (selected != null)
                {
                    Id.Text = selected.branch_id;
                    BranchName.Text = selected.name;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
