﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using PrintReceiptBL;
using PrintReceiptDAL;
using DomainModel;

namespace PrintReceipt.Forms
{
    /// <summary>
    /// Interaction logic for CustomerWindow.xaml
    /// </summary>
    public partial class CustomerPage : Page
    {
        public CustomerPage()
        {
            InitializeComponent();
            Blinding();
        }

        private void Blinding()
        {
            try
            {
                CusDataGrid.ItemsSource = null;
                CusDataGrid.ItemsSource = BussinessLayerService.CustomerInfo().GetAllCustomerInfo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void CusDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (cus_rcp)CusDataGrid.SelectedItem;
                if (selected != null)
                {
                    IdTextBox.Text = selected.cus_id;
                    //Find selected customer
                    var cus_selected = (cus_rcp)BussinessLayerService.CustomerInfo().GetCustomerInfo(selected.cus_id.ToString());
                    {
                        NameTextBox.Text = cus_selected.name;
                        AddressTextBox.Text = cus_selected.homeid;
                        SoiTextBox.Text = cus_selected.soi;
                        MooTextBox.Text = cus_selected.moo;
                        RoadTextBox.Text = cus_selected.road;
                        TumbolTextBox.Text = cus_selected.tumbol;
                        AmphurTextBox.Text = cus_selected.amphur;
                        ProvinceTextBox.Text = cus_selected.province;
                        PostTextBox.Text = cus_selected.postal;
                        TelTextBox.Text = cus_selected.tel;
                        FaxTextBox.Text = cus_selected.fax;
                        TaxidTextBox.Text = cus_selected.taxid;
                        SsidTextBox.Text = cus_selected.ssid;
                        RemarkTextBox.Text = cus_selected.remark;
                    }

                    UpdateButton.IsEnabled = true;
                    DeleteButton.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(IdTextBox.Text)) throw new Exception("กรุณากดปุ่ม Clear ข้อมูลก่อนการบันทึกรายชื่อใหม่");

                if (string.IsNullOrEmpty(NameTextBox.Text)) throw new Exception("กรุณากรอกข้อมูลชื่อ ก่อนการบันทึก");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var newItem = new cus_rcp
                    {
                        //Get latest id
                        cus_id = BussinessLayerService.CustomerInfo().GetLatestCusID(),
                        name = NameTextBox.Text,
                        homeid = AddressTextBox.Text,
                        soi = SoiTextBox.Text,
                        moo = MooTextBox.Text,
                        road = RoadTextBox.Text,
                        tumbol = TumbolTextBox.Text,
                        amphur = AmphurTextBox.Text,
                        province = ProvinceTextBox.Text,
                        postal = PostTextBox.Text,
                        tel = TelTextBox.Text,
                        fax = FaxTextBox.Text,
                        taxid = TaxidTextBox.Text,
                        ssid = SsidTextBox.Text,
                        remark = RemarkTextBox.Text,
                        recorddt = DateTime.Now
                    };

                    BussinessLayerService.CustomerInfo().AddNewCustomer(newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    Blinding();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearInput();
            UpdateButton.IsEnabled = false;
            DeleteButton.IsEnabled = false;
        }

        private void ClearInput()
        {
            IdTextBox.Text = "";
            NameTextBox.Text = "";
            AddressTextBox.Text = "";
            SoiTextBox.Text = "";
            MooTextBox.Text = "";
            RoadTextBox.Text = "";
            TumbolTextBox.Text = "";
            AmphurTextBox.Text = "";
            ProvinceTextBox.Text = "";
            PostTextBox.Text = "";
            TelTextBox.Text = "";
            FaxTextBox.Text = "";
            TaxidTextBox.Text = "";
            SsidTextBox.Text = "";
            RemarkTextBox.Text = "";
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var selected = (cus_rcp)CusDataGrid.SelectedItem;
                    if (selected != null)
                    {
                        BussinessLayerService.CustomerInfo().DeleteCustomer(selected);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    Blinding();
                    ClearInput();                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(NameTextBox.Text)) throw new Exception("กรุณากรอกข้อมูลชื่อ ก่อนการบันทึก");
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var existedItem = new cus_rcp
                    {
                        //Get latest id
                        cus_id = IdTextBox.Text,
                        name = NameTextBox.Text,
                        homeid = AddressTextBox.Text,
                        soi = SoiTextBox.Text,
                        moo = MooTextBox.Text,
                        road = RoadTextBox.Text,
                        tumbol = TumbolTextBox.Text,
                        amphur = AmphurTextBox.Text,
                        province = ProvinceTextBox.Text,
                        postal = PostTextBox.Text,
                        tel = TelTextBox.Text,
                        fax = FaxTextBox.Text,
                        taxid = TaxidTextBox.Text,
                        ssid = SsidTextBox.Text,
                        remark = RemarkTextBox.Text,
                        recorddt = DateTime.Now
                    };
                    BussinessLayerService.CustomerInfo().UpdateCustomer(existedItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    Blinding();
                }
            }
            catch
            {

            }
        }
    }
}
