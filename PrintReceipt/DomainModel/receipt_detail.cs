﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class receipt_detail
    {
        public string receipt_no { get; set; }
        public byte no { get; set; }
        public string des { get; set; }
        public Nullable<int> quantity { get; set; }
        public Nullable<decimal> punit { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<System.DateTime> recorddt { get; set; }
    }
}
