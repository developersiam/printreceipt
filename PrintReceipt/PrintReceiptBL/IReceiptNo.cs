﻿using System.Collections.Generic;
using DomainModel;
using PrintReceiptDAL;
using System.Linq;
using System.Globalization;
using System;

namespace PrintReceiptBL
{
    public interface IReceiptNo
    {
        receipt_no GetReceiptNo();
        void UpdateReceiptNo(receipt_no item);
    }
    public class ReceiptNoBL : IReceiptNo
    {
        public receipt_no GetReceiptNo()
        {
            //Check existed current thai year
            int curr_yy = DateTime.Now.Year;
            curr_yy = curr_yy + 543;
            string convert_yy = curr_yy.ToString();
            var ex = DataAccessLayerService.ReceiptNoInfoRepository().GetSingle(a => a.yy == convert_yy);
            if (ex == null) AddNewYear(convert_yy);

            //update & return current year
            return DataAccessLayerService.ReceiptNoInfoRepository().GetSingle(a => a.yy == convert_yy);

        }
        public void AddNewYear(string curr_yy)
        {
            //remove latest year first
            DeleteLastYear(curr_yy);

            receipt_no newInfo = new receipt_no()
            {
                yy = curr_yy,
                receipt_no1 = "0000",
                recorddt = DateTime.Now
            };
            DataAccessLayerService.ReceiptNoInfoRepository().Add(newInfo);
        }
        public void DeleteLastYear(string last_yy)
        {
            int tmpyy = Convert.ToInt32(last_yy) - 1;
            last_yy = tmpyy.ToString();

            receipt_no lstInfo = new receipt_no()
            {
                yy = last_yy
            };
            DataAccessLayerService.ReceiptNoInfoRepository().Remove(lstInfo);
        }
        public void UpdateReceiptNo(receipt_no item)
        {
            DataAccessLayerService.ReceiptNoInfoRepository().Update(item);
        }
    }
}