﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintReceiptBL
{
    public static class BussinessLayerService
    {
        public static IBank BankInfo()
        {
            IBank obj = new BankBL();
            return obj;
        }
        public static IBranch BranchInfo()
        {
            IBranch obj = new BranchBL();
            return obj;
        }

        public static IReceipt ReceiptInfo()
        {
            IReceipt obj = new ReceiptBL();
            return obj;
        }
        public static IReceiptDetail ReceiptDetailInfo()
        {
            IReceiptDetail obj = new ReceiptDetailBL();
            return obj;
        }
        public static ICusRcp CustomerInfo()
        {
            ICusRcp obj = new CustomerBL();
            return obj;
        }
        public static IReceiptNo ReceiptNoInfo()
        {
            IReceiptNo obj = new ReceiptNoBL();
            return obj;
        }
    }
}
