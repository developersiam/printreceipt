﻿using System.Collections.Generic;
using DomainModel;
using PrintReceiptDAL;
using System.Linq;
using System;
using System.Globalization;

namespace PrintReceiptBL
{
    public interface IBank
    {
        List<bank> GetAllBankInfo();
        bool ExistedBank(string id);
        bank GetBankInfo(string text);
        void UpdateBank(bank existedBank);
        string GetMaxBankID();
        void AddNewBank(bank newBank);
        List<string> GetBankName();
        string GetBankIDByName(string bName);
    }

    public class BankBL : IBank
    {
        public List<bank> GetAllBankInfo()
        {
            return DataAccessLayerService.BankInfoRepository().GetAll().ToList();
        }

        public bool ExistedBank(string Bankid)
        {
            bool existed = false;
            bankRepository bankinfo = new bankRepository();
            var b = bankinfo.GetList(c => c.bank_id == Bankid);
            if (b.Count() > 0) existed = true;

            return existed;
        }
        public bank GetBankInfo(string Bankid)
        {
            return DataAccessLayerService.BankInfoRepository().GetSingle(b => b.bank_id == Bankid);
        }
        public void UpdateBank(bank item)
        {
            DataAccessLayerService.BankInfoRepository().Update(item);
        }
        public void AddNewBank(bank item)
        {
            DataAccessLayerService.BankInfoRepository().Add(item);
        }
        public string GetMaxBankID()
        {
            int maxID = 0;
            string tmpmax = "";
            string maxchar = "";
            bankRepository p = new bankRepository();
            tmpmax = p.GetAll().GroupBy(r => r.bank_id).OrderByDescending(o => o.Key).First().Select(c => c.bank_id).First();

            maxID = (int.Parse(tmpmax, NumberStyles.Any, CultureInfo.InvariantCulture)) + 1 ;
            maxchar = maxID.ToString().PadLeft(3, '0');
            return maxchar;
        }
        public List<string> GetBankName()
        {
            bankRepository p = new bankRepository();
            var bankName = new List<string>();
            bankName = p.GetAll().OrderBy(c => c.name).Select(w => w.name).ToList();
            return bankName;
        }
        public string GetBankIDByName(string bName)
        {
            string Bank_id = "";
            var Existed_name = DataAccessLayerService.BankInfoRepository().GetSingle(e => e.name == bName);
            if (Existed_name != null) Bank_id = Existed_name.bank_id.ToString();

            return Bank_id;
        }


    }
}