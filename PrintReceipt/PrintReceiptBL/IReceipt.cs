﻿using System.Collections.Generic;
using DomainModel;
using PrintReceiptDAL;
using System.Linq;
using System.Globalization;

namespace PrintReceiptBL
{
    public interface IReceipt
    {
        List<string> GetReceiptNo();
        receipt GetReceiptInfo(string receipt_no);
        void AddNewReceipt(receipt newItem);
        void UpdateReceipt(receipt exItem);
        void DeleteReceipt(receipt exItem);
        bool ExistedReceipt(string rec_no); 
    }

    public class ReceiptBL : IReceipt
    {
        public List<string> GetReceiptNo()
        {
            receiptRepository p = new receiptRepository();
            var rName = new List<string>();
            rName = p.GetAll().OrderByDescending(c => c.receipt_no).Select(w => w.receipt_no).ToList();
            return rName;
        }
        public receipt GetReceiptInfo(string rc_no)
        {
            return DataAccessLayerService.ReceiptInfoRepository().GetSingle(r => r.receipt_no == rc_no);
        }
        public void AddNewReceipt(receipt item)
        {
            DataAccessLayerService.ReceiptInfoRepository().Add(item);
        }
        public void UpdateReceipt(receipt exItem)
        {
            DataAccessLayerService.ReceiptInfoRepository().Update(exItem);
        }
        public void DeleteReceipt(receipt exItem)
        {
            DataAccessLayerService.ReceiptInfoRepository().Remove(exItem);
        }
        public bool ExistedReceipt(string recNo)
        {
            bool ex = false;
            var rcInfo = DataAccessLayerService.ReceiptInfoRepository().GetSingle(r => r.receipt_no == recNo);
            if (rcInfo != null) ex = true;

            return ex;
        }
    }
}
