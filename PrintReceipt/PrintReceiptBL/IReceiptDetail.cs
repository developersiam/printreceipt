﻿using System.Collections.Generic;
using DomainModel;
using PrintReceiptDAL;
using System.Linq;
using System.Globalization;
using System;

namespace PrintReceiptBL
{
    public interface IReceiptDetail
    {
        List<receipt_detail> GetReceiptDetailInfo(string rNo);
        void DeletedReceiptDetails(receipt_detail exItem);
        void AddReceiptDetails(receipt_detail ItemList);
        bool ChkExsitedInfo(string rNo, byte iNo);       
    }
    public class ReceiptDetailBL : IReceiptDetail
    {

        public List<receipt_detail> GetReceiptDetailInfo(string rNo)
        {
            receiptDetailRepository rDetailinfo = new receiptDetailRepository();
            List<receipt_detail> rDetails = new List<receipt_detail>();
            var b = rDetailinfo.GetList(c => c.receipt_no == rNo).ToList();
            return b;
        }
        public void DeletedReceiptDetails(receipt_detail item)
        {
           DataAccessLayerService.ReceiptDetailInfoRepository().Remove(item);           
        }
        public void AddReceiptDetails(receipt_detail i)
        {
            var rcDetails = DataAccessLayerService.ReceiptDetailInfoRepository().GetSingle(d => d.receipt_no == i.receipt_no && d.no == i.no);
            if (rcDetails != null)
            {
                DataAccessLayerService.ReceiptDetailInfoRepository().Update(i);
            }
            else
                DataAccessLayerService.ReceiptDetailInfoRepository().Add(i);
        }
        public bool ChkExsitedInfo(string rNo, byte iNo)
        {
            bool ex = false;
            var rcInfo = DataAccessLayerService.ReceiptDetailInfoRepository().GetSingle(w => w.receipt_no == rNo && w.no == iNo);
            if (rcInfo != null) ex = true;

            return ex;
        }
    }
}
