﻿using System.Collections.Generic;
using DomainModel;
using PrintReceiptDAL;
using System.Linq;
using System.Globalization;
using System;

namespace PrintReceiptBL
{
    public interface ICusRcp
    {
        List<cus_rcp> GetAllCustomerInfo();
        cus_rcp GetCustomerInfo(string text);
        void AddNewCustomer(cus_rcp newItem);
        string GetLatestCusID();
        void DeleteCustomer(cus_rcp Item);
        void UpdateCustomer(cus_rcp Item);     
    }
    public class CustomerBL : ICusRcp
    {
        public List<cus_rcp> GetAllCustomerInfo()
        {
            return DataAccessLayerService.CustomerInfoRepository().GetAll().ToList();
        }
        public cus_rcp GetCustomerInfo(string Custid)
        {
            return DataAccessLayerService.CustomerInfoRepository().GetSingle(b => b.cus_id == Custid);
        }
        public void AddNewCustomer(cus_rcp newItem)
        {
            DataAccessLayerService.CustomerInfoRepository().Add(newItem);
        }
        public string GetLatestCusID()
        {
            int tmpId = 0;
            var i = DataAccessLayerService.CustomerInfoRepository().GetAll().Select(x => x.cus_id).Max();
            tmpId = Convert.ToInt32(i) + 1;

            return tmpId.ToString();
        }
        public void DeleteCustomer(cus_rcp Item)
        {
            DataAccessLayerService.CustomerInfoRepository().Remove(Item);
        }
        public void UpdateCustomer(cus_rcp Item)
        {
            DataAccessLayerService.CustomerInfoRepository().Update(Item);
        }
    }
}
