﻿using System.Collections.Generic;
using DomainModel;
using PrintReceiptDAL;
using System.Linq;
using System.Globalization;

namespace PrintReceiptBL
{
    public interface IBranch
    {
        List<branch> GetAllBranchInfo();
        bool ExistedBranch(string id);
        branch GetBranchInfo(string text);
        void UpdateBranch(branch existedBranch);
        string GetMaxBranchID();
        void AddNewBranch(branch newBranch);
        List<string> GetBranchName();
        string GetBranchIDByName(string bName);
    }
    public class BranchBL : IBranch
    {
        public List<branch> GetAllBranchInfo()
        {
            return DataAccessLayerService.BranchInfoRepository().GetAll().ToList();
        }
        public bool ExistedBranch(string Branchid)
        {
            bool existed = false;
            branchRepository branchinfo = new branchRepository();
            var b = branchinfo.GetList(c => c.branch_id == Branchid);
            if (b.Count() > 0) existed = true;

            return existed;
        }
        public branch GetBranchInfo(string Branchid)
        {
            return DataAccessLayerService.BranchInfoRepository().GetSingle(b => b.branch_id == Branchid);
        }
        public void UpdateBranch(branch item)
        {
            DataAccessLayerService.BranchInfoRepository().Update(item);
        }
        public string GetMaxBranchID()
        {
            int maxID = 0;
            string tmpmax = "";
            string maxchar = "";
            branchRepository p = new branchRepository();
            tmpmax = p.GetAll().GroupBy(r => r.branch_id).OrderByDescending(o => o.Key).First().Select(c => c.branch_id).First();

            maxID = (int.Parse(tmpmax, NumberStyles.Any, CultureInfo.InvariantCulture)) + 1;
            maxchar = maxID.ToString().PadLeft(3, '0');
            return maxchar;
        }
        public void AddNewBranch(branch item)
        {
            DataAccessLayerService.BranchInfoRepository().Add(item);
        }
        public List<string> GetBranchName()
        {
            branchRepository p = new branchRepository();
            var bName = new List<string>();
            bName = p.GetAll().OrderBy(c => c.name).Select(w => w.name).ToList();
            return bName;
        }
        public string GetBranchIDByName(string bName)
        {
            string Branch_id = "";
            var Existed_name = DataAccessLayerService.BranchInfoRepository().GetSingle(e => e.name == bName);
            if (Existed_name != null) Branch_id = Existed_name.branch_id.ToString();

            return Branch_id;
        }
    }
}