﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PrintReceipt
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BankMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainFrame.Navigate(new Forms.BankPage());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Message: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ExitMenu_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("ต้องการออกจากโปรแกรมใช่หรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) Close();
        }

        private void BranchMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainFrame.Navigate(new Forms.BranchPage());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Message: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void PrintMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainFrame.Navigate(new Forms.ReceiptPage());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Message: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private void CustomerMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainFrame.Navigate(new Forms.CustomerPage());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Message: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
